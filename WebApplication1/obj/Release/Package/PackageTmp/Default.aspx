﻿<%@ Page Title="首页" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>工程师查询管理系统</h1>
        <p class="lead">基于ASP.NET的工程师查询管理系统</p>
        <p>&nbsp;<asp:Button ID="Button1" runat="server" class="btn btn-primary btn-lg" OnClick="Button1_Click" Text="立即开始" />
        </p>
        
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>程序架构</h2>
            <p>
                ASP.NET Web Forms 架构</p>
            <p>
                
                

            </p>
        </div>
        <div class="col-md-4">
            <h2>后端信息</h2>
            <p>
                使用IIS作为服务器后端</p>
            <p>
                &nbsp;</p>
        </div>
        <div class="col-md-4">
            <h2>数据库</h2>
            <p>使用SQL Server作为后台服务器</p>
            <p>
                &nbsp;</p>
        </div>
    </div>

</asp:Content>
