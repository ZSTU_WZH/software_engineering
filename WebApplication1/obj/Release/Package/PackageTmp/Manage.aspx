﻿<%@ Page Title="管理" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Manage.aspx.cs" Inherits="WebApplication1.Manage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>管理</h2>
    <h3>管理数据库（管理员权限）</h3>
<p></p>
<asp:Panel ID="Panel1" runat="server">
    <div style="margin-top:15px">
    <asp:Label ID="Label2" runat="server" Text="姓名：（文本型）"></asp:Label>
    <asp:TextBox ID="TextBox1" runat="server" MaxLength="8"></asp:TextBox>
    </div>
    <br />
    <div style="margin-top:15px">
    <asp:Label ID="Label3" runat="server" Text="编号：（文本型）"></asp:Label>
    <asp:TextBox ID="TextBox2" runat="server" OnTextChanged="TextBox2_TextChanged" MaxLength="4"></asp:TextBox>
    </div>
    <br />
    <div style="margin-top:15px">
    <asp:Label ID="Label4" runat="server" Text="性别：（文本型）"></asp:Label>
    <asp:TextBox ID="TextBox3" runat="server" MaxLength="2"></asp:TextBox>
    </div>
    <br />
    <div style="margin-top:15px">
    <asp:Label ID="Label5" runat="server" Text="地址：（文本型）"></asp:Label>
    <asp:TextBox ID="TextBox4" runat="server" MaxLength="30"></asp:TextBox>
    </div>
    <br />
    <div style="margin-top:15px">
    <asp:Label ID="Label6" runat="server" Text="电话：（文本型）"></asp:Label>
    <asp:TextBox ID="TextBox5" runat="server" MaxLength="13"></asp:TextBox>
    </div>
    <br />
    <div style="margin-top:15px">
    <asp:Label ID="Label7" runat="server" Text="工龄：（整数型）"></asp:Label>
    <asp:TextBox ID="TextBox6" runat="server" MaxLength="3"></asp:TextBox>
    </div>
    <br />
    <div style="margin-top:15px">
    <asp:Label ID="Label8" runat="server" Text="基本薪水：文本："></asp:Label>
    <asp:TextBox ID="TextBox7" runat="server" MaxLength="20"></asp:TextBox>
    </div>
    <br />
    <div style="margin-top:15px">
    <asp:Label ID="Label9" runat="server" Text="生日：（日期型）"></asp:Label>
    <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
        </div>
    <br />
    <div style="margin-top:15px">
    <asp:Label ID="Label10" runat="server" Text="学历：（文本型）"></asp:Label>
    <asp:TextBox ID="TextBox9" runat="server" MaxLength="30"></asp:TextBox>
        </div>
    <br />
    <div style="margin-top:15px">
    <asp:Button ID="Button3" runat="server" Text="增加" OnClick="Button3_Click" />
    <asp:Button ID="Button4" runat="server" Text="删除" OnClick="Button4_Click" />
    <asp:Button ID="Button5" runat="server" Text="修改" OnClick="Button5_Click" />
        <asp:Label ID="Label_at" runat="server"></asp:Label>
        </div>
    <br />
</asp:Panel>
    <p style="margin:15px">所有数据：<asp:Label ID="Label_st" runat="server" ForeColor="Red"></asp:Label>
    </p>
    <p>
        <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None"  Width="90%" OnSelectedIndexChanging="GridView1_SelectedIndexChanging1">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#337AB7" Font-Bold="True" ForeColor="White" Height="50px" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" Height="50px" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
</asp:Content>
