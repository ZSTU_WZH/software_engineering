﻿using System;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["Userinfo"] != null)
            {
                Button2.Visible = true;
                string name = Request.Cookies["Userinfo"]["UserName"];
                string ca = Request.Cookies["Userinfo"]["ca"];
                string permission;
                if (ca == "1")
                {
                    permission = "管理员";

                }
                else
                    permission = "访客";
                Label1.Text = permission+" "+name+"  已登录";
 
            }
            else
            {
                Button2.Visible = false;
                Label1.Text = "  未登录";
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (Request.Cookies["Userinfo"] != null)
            {
                
                Response.Cookies["Userinfo"].Expires = DateTime.Now.AddDays(-1);
                Response.Redirect("Default.aspx");
                Button2.Visible = false;
            }
            Button2.Visible = false;
        }
    }
}