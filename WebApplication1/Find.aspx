﻿<%@ Page Title="查询" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Find.aspx.cs" Inherits="WebApplication1.Find" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>查询</h2>
    <h3>在数据库中查询数据</h3>
    <div>
        
        <div style="margin-top:15px"><asp:Label ID="Label2" runat="server"  Text="精准查询内容(可查询除了工龄和生日之外的所有数据)： "></asp:Label></div>
        <div style="margin-right:15px;margin-top:15px"><asp:TextBox ID="TextBox1" runat="server" CssClass="form-control"></asp:TextBox></div>
        <div style="margin-right:15px;margin-top:15px"><asp:Button ID="Button3" runat="server" Text="立即查询" OnClick="Button3_Click" />
        <div style="margin-right:15px;margin-top:15px"> <asp:Button ID="Button4" runat="server" Text="显示全部" OnClick="Button4_Click" />
            <br />
            <br />
            查询结果（默认为全部显示）：<br />
            <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None"  OnSelectedIndexChanged="GridView1_SelectedIndexChanged" Width="100%">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#337AB7" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" Height="50px" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Left" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" HorizontalAlign="Left" Height="50px" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:EngineerConnectionString %>" SelectCommand="SELECT * FROM [Eng_info]"></asp:SqlDataSource>
        </div>
            
    </div>
    
</div>
</asp:Content>
