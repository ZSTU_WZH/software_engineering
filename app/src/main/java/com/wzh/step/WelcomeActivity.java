package com.wzh.step;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.wzh.frame.BaseActivity;

/**
 * className:Welcome
 * Author:WZH
 * Time:2022-01-07
 */
public class WelcomeActivity extends BaseActivity {

    private Handler handler;
    private Runnable jumpRunnable;

    @Override
    protected void onInitVariable() {
        handler = new Handler(Looper.getMainLooper());
        jumpRunnable =new Runnable() {
            @Override
            public void run() {
                //跳转到home
                Intent intent = new Intent();
                intent.setClass(WelcomeActivity.this,HomeActivity.class);
                startActivity(intent);
                WelcomeActivity.this.finish();
            }
        };
    }

    @Override
    protected void onInitView(Bundle savedInstanceState) {
        setContentView(R.layout.act_welcome);
    }

    @Override
    protected void onRequestData() {
        handler.postDelayed(jumpRunnable,3000);
    }
}
