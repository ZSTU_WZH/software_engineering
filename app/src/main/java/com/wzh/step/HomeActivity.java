package com.wzh.step;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.wzh.frame.BaseActivity;


/**
 * className:Welcome
 * Author:WZH
 * Time:2022-01-06
 */
public class HomeActivity extends BaseActivity {


    @Override
    protected void onInitVariable() {

    }

    @Override
    protected void onInitView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_home);
    }

    @Override
    protected void onRequestData() {

    }
}