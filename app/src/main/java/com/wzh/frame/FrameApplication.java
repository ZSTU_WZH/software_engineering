package com.wzh.frame;

import android.app.Activity;
import android.app.Application;

import java.util.LinkedList;

/**
 * className:FrameApplication
 * Author:WZH
 * Time:2022-01-06
 */
public class FrameApplication extends Application {
    private static LinkedList<Activity> actList =new LinkedList<Activity>();

    public LinkedList<Activity> getActList(){
        return actList;
    }

    /**
     * 添加到列表
     * @param act
     */
    public static void addToActivityList(final Activity act){
        if(act!=null){
            actList.add(act);
        }
    }

    /**
     * 删除act
     * @param act
     */
    public static void removeFromActivityList(final Activity act){
        if(act!=null && actList.size()>0 && actList.indexOf(act)!=-1){
            actList.remove(act);
        }
    }

    /**
     * 清空所有的activity
     */
    public static void clearActivityList(){
        for(int i=actList.size()-1;i>=0;i--){
            final Activity act = actList.get(i);
            if(act!=null){
                act.finish();
            }
        }
    }

    public static void exitApp(){

        try {
            clearActivityList();

        }catch (final Exception e){

        }finally {
            System.exit(0);
            android.os.Process.killProcess(android.os.Process.myPid());
        }

    }

    private PrefsManger prefsManger;
    private static FrameApplication instance;

    public static FrameApplication getInstance() {
        return instance;
    }

    public PrefsManger getPrefsManger() {
        return prefsManger;
    }



    private ErrorHandler errorHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        prefsManger = new PrefsManger(this);
        errorHandler = ErrorHandler.getInstance();
    }
}

