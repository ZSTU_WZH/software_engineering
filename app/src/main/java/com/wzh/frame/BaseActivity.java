package com.wzh.frame;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.fragment.app.FragmentActivity;

/**
 * className:BaseActivity
 * Author:WZH
 * Time:2022-01-07
 */
public abstract  class BaseActivity extends FragmentActivity {

    //是否显示程序标题
    protected boolean isHideAppTitle =true;
    protected boolean isHideSysTitle = false;


    public void onCreate(Bundle saveInstanceState){
        this.onInitVariable();
        if(this.isHideAppTitle)
        {
            this.requestWindowFeature(Window.FEATURE_NO_TITLE); //hide app title
        }
        super.onCreate(saveInstanceState);
        if(this.isHideSysTitle)
        {
         this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                 WindowManager.LayoutParams.FLAG_FULLSCREEN );
        }
        //构造view，绑定事件
        this.onInitView(saveInstanceState);
        //请求数据
        this.onRequestData();
        FrameApplication.addToActivityList(this);
    }

    protected void onDestroy(){
        FrameApplication.removeFromActivityList(this);
        super.onDestroy();
    }

    /**
     * 1)初始化变量 最先被调用
     */

    protected  abstract void onInitVariable();
    /**
     * 2)初始化UI 布局载入操作
     * @param savedInstanceState
     */
    protected abstract void onInitView(final Bundle savedInstanceState);

    /**
     * 3)请求数据
     */

    protected abstract void onRequestData();

}
