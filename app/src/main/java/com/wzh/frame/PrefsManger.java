package com.wzh.frame;

import android.content.Context;

/**
 * className:PrefsManger
 * Author:WZH
 * Time:2022-01-06
 */
public class PrefsManger {
    private final Context mContext;
    private static final String PREFERENCE_NAME="wzh_step";
    public PrefsManger(final Context context){
        this.mContext=context;
    }
    public void clear(){
        mContext.getSharedPreferences(PrefsManger.PREFERENCE_NAME,Context.MODE_PRIVATE).edit().clear().commit();
    }

    public boolean contains(){
         return  mContext.getSharedPreferences(PrefsManger.PREFERENCE_NAME,Context.MODE_PRIVATE)
                 .contains(PrefsManger.PREFERENCE_NAME);

    }

    public boolean getBoolean(final String key){
        return this.mContext.getSharedPreferences
                (PrefsManger.PREFERENCE_NAME,Context.MODE_PRIVATE).getBoolean(key,false);
    }

    public boolean getBooleanDefaultTrue(final String key){
        return this.mContext.getSharedPreferences
                (PrefsManger.PREFERENCE_NAME,Context.MODE_PRIVATE).getBoolean(key,true);
    }

    public boolean putBoolean(final String key,final boolean value){
        return mContext.getSharedPreferences(PrefsManger.PREFERENCE_NAME,Context.MODE_PRIVATE)
                .edit().putBoolean(key, value).commit();
    }



}
