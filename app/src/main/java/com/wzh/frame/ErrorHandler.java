package com.wzh.frame;

import android.content.Context;

/**
 * className:ErrorHandler
 * Author:WZH
 * Time:2022-01-07
 */
public class ErrorHandler implements Thread.UncaughtExceptionHandler {
    public void uncaughtException(final Thread thread,final Throwable ex)
    {
        LogWriter.LogToFile(true,"Error","崩溃信息"+ex.getMessage());
        LogWriter.LogToFile(true,"Error","崩溃线程名称"+thread.getName()+"线程ID："+thread.getId());
        final StackTraceElement[] trace = ex.getStackTrace();
        for(final StackTraceElement element:trace){
            LogWriter.logToFile(true,"Line:"+element.getLineNumber()+":"+element.getMethodName());
        }
        ex.printStackTrace();
        FrameApplication.exitApp();
    }
    private static ErrorHandler instance;

    //单例
    public static ErrorHandler getInstance() {
        if(ErrorHandler.instance==null)
        {
            ErrorHandler.instance = new ErrorHandler();
        }
        return ErrorHandler.instance;
    }

    private ErrorHandler(){}

    public void setErrorHandler(final Context ctx){
            Thread.setDefaultUncaughtExceptionHandler(this);
        }



    }


